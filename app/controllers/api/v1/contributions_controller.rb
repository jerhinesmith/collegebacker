class Api::V1::ContributionsController < Api::V1::BaseController
  MAX_PER_PAGE = 6
  before_action :set_fund
  before_action :set_contribution, only: [:destroy]

  def index
    contributions = @fund.contributions.page(page).per(MAX_PER_PAGE)

    # Set the http headers
    create_pagination_headers(contributions)

    render json: contributions, each_serializer: Api::V1::ContributionSerializer
  end

  def create
    contribution = @fund.contributions.create(contribution_params)

    if contribution.save
      head :no_content
    else
      render json: { errors: contribution.errors }, success: false, status: :unprocessable_entity
    end
  end

  def destroy
    if @contribution.destroy
      head :no_content
    else
      render json: { errors: @contribution.errors }, success: false, status: :unprocessable_entity
    end
  end

  private

  def set_fund
    @fund = current_user.funds.find(params[:fund_id])
  end

  def set_contribution
    @contribution = @fund.contributions.find(params[:id])
  end

  def contribution_params
    params.require(:contribution).permit(:user_id, :amount_cents)
  end
end
