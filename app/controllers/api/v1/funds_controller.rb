class Api::V1::FundsController < Api::V1::BaseController
  def index
    funds = current_user.funds

    render json: funds, each_serailizer: Api::V1::FundSerializer
  end
end
