class Api::V1::SessionsController < Api::V1::BaseController
  skip_before_action :authenticate

  def create
    # Attempt to login with Sorcery
    if (user = login(params[:session][:email], params[:session][:password]))
      render json: user, serializer: Api::V1::UserSessionSerializer
    else
      head :unauthorized
    end
  end

  def destroy
    logout

    head :no_content
  end
end
