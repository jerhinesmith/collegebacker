class ApplicationController < ActionController::Base
  include Sorcery::Controller

  def page
    Integer(params[:page] || 1)
  end

  protected

  def authenticate
    authenticate_token || require_login
  end

  def authenticate_token
    authenticate_with_http_token do |token, _options|
      # rubocop:disable Lint/AssignmentInCondition
      if user = User.find_by(authentication_token: token)
        auto_login(user)
      end
      # rubocop:enable Lint/AssignmentInCondition
    end
  end

  def create_pagination_headers(scope)
    request_params = request.query_parameters
    url_without_params = pagination_base_url

    pagination_links = []
    pagination_page_links(scope).each do |k, v|
      new_request_hash = request_params.merge(page: v)
      pagination_links << "<#{url_without_params}?#{new_request_hash.to_param}>; rel=\"#{k}\""
    end
    headers['Link'] = pagination_links.join(', ')
    headers['X-Total-Count'] = scope.total_count
  end

  def pagination_base_url
    url_without_params = request.original_url.slice(0..(request.original_url.index('?') - 1)) unless request.query_parameters.empty?
    url_without_params.presence || request.original_url
  end

  def pagination_page_links(scope)
    {}.tap do |page|
      page[:first] = 1
      page[:last] = scope.total_pages
      page[:next] = scope.current_page + 1 unless scope.last_page?
      page[:prev] = scope.current_page - 1 unless scope.first_page?
    end
  end
end
