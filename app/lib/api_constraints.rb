# http://railscasts.com/episodes/350-rest-api-versioning

class ApiConstraints
  VENDOR_PREFIX = 'cb'.freeze

  attr_accessor :version, :default

  def initialize(attributes = {})
    raise ArgumentError, 'No version specified or version of correct type' unless attributes[:version].to_i.positive?

    self.version = attributes[:version]
    self.default = attributes.fetch(:default, false)
  end

  def matches?(req)
    if req.headers['Accept'].match?(%r{application\/vnd\.#{VENDOR_PREFIX}\.v[0-9]+})
      req.headers['Accept'].include?("application/vnd.#{VENDOR_PREFIX}.v#{version}")
    else
      default
    end
  end
end
