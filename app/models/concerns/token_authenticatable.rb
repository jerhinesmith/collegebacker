module TokenAuthenticatable
  extend ActiveSupport::Concern

  included do
    before_save :ensure_authentication_token!

    def ensure_authentication_token!
      self.authentication_token = generate_authentication_token if authentication_token.blank?
    end

    def generate_authentication_token
      loop do
        token = SecureRandom.urlsafe_base64(25).tr('lIO0', 'sxyz')
        break token unless self.class.exists?(authentication_token: token)
      end
    end

    def reset_authentication_token!
      update_attribute(:authentication_token, generate_authentication_token)
    end
  end

  module ClassMethods; end
end
