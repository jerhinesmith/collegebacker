class Contribution < ApplicationRecord
  monetize :amount_cents

  # Associations
  belongs_to :fund
  belongs_to :contributor, class_name: 'User', foreign_key: :user_id

  # Validation
  validates :user_id, :fund_id, :amount_cents, presence: true
  validates :amount_cents, numericality: { greater_than: 0 }
end
