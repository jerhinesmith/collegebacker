class Fund < ApplicationRecord
  belongs_to :user, inverse_of: :funds
  has_many :contributions, dependent: :destroy, inverse_of: :fund
  has_many :contributors, through: :contributions

  # Validations
  validates :user_id, :name, presence: true
end
