class User < ApplicationRecord
  authenticates_with_sorcery!
  mount_uploader :phoo, ::PhotoUploader
  include TokenAuthenticatable

  # Associations
  has_many :funds, dependent: :destroy, inverse_of: :user
  has_many :contributions, dependent: :destroy, inverse_of: :contributor

  # Validations
  validates :password, length: { minimum: 3 }, if: -> { new_record? || changes[:crypted_password] }
  validates :password, confirmation: true, if: -> { new_record? || changes[:crypted_password] }
  validates :password_confirmation, presence: true, if: -> { new_record? || changes[:crypted_password] }
  validates :email, uniqueness: true
  validates :first_name, :last_name, presence: true
end
