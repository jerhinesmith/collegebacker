class Api::V1::ContributionSerializer < ActiveModel::Serializer
  attributes  :id,
              :amount_cents

  belongs_to :contributor, serializer: Api::V1::ContributorSerializer
end
