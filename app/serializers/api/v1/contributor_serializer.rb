class Api::V1::ContributorSerializer < ActiveModel::Serializer
  attributes  :id,
              :first_name,
              :last_name,
              :photo

  def photo
    object.photo&.url
  end
end
