class Api::V1::FundSerializer < ActiveModel::Serializer
  attributes  :id,
              :name
end
