class Api::V1::UserSessionSerializer < ActiveModel::Serializer
  attributes  :id,
              :email,
              :first_name,
              :last_name,
              :photo,
              :authentication_token

  def photo
    object.photo&.url
  end
end
