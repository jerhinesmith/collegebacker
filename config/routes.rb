require 'api_constraints'

Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :api, defaults: { format: :json } do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
      post   'login'  => 'sessions#create'
      delete 'logout' => 'sessions#destroy'

      resources :funds, only: %i[index] do
        resources :contributions, only: %i[index create destroy]
      end
    end
  end
end
