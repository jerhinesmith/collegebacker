class CreateContributions < ActiveRecord::Migration[5.2]
  def change
    create_table :contributions do |t|
      t.references :fund, foreign_key: true
      t.references :user, foreign_key: true
      t.integer :amount_cents, null: false, default: 0

      t.timestamps
    end
  end
end
