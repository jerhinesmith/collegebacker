FactoryBot.define do
  factory :contribution do
    # Associations
    association :contributor, factory: :user
    fund

    # Attributes
    amount_cents { rand(1..1_000_000) }
  end
end
