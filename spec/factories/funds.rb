FactoryBot.define do
  factory :fund do
    # Associations
    user

    # Attributes
    name { Faker::Lorem.sentence }
  end
end
