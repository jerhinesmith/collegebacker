FactoryBot.define do
  factory :user do
    # Attributes
    email { Faker::Internet.safe_email }
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    password 'secret'
    password_confirmation 'secret'
  end
end
