require 'rails_helper'

RSpec.describe Contribution, type: :model do
  it 'has a valid factory' do
    expect(FactoryBot.build_stubbed(:contribution)).to be_valid
  end
end
