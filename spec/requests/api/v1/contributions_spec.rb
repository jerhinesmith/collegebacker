require 'rails_helper'

describe 'api/v1/contributions', type: :request do
  let(:current_user) { FactoryBot.create(:user) }
  let(:contributor) { FactoryBot.create(:user) }
  let(:fund) { FactoryBot.create(:fund, user: current_user) }

  before(:each) do
    set_user current_user
    set_api_version 1

    7.times do
      FactoryBot.create(:contribution, fund: fund, contributor: contributor)
    end
  end

  context 'index' do
    let(:url) { "/api/funds/#{fund.id}/contributions" }

    it 'json' do
      get url

      expect(response).to be_successful
      expect(json.length).to eq 6
    end
  end

  context 'create' do
    let(:url) { "/api/funds/#{fund.id}/contributions" }

    it 'successful JSON' do
      post url, params: {
        contribution: { user_id: contributor.id, amount_cents: 10_000 }
      }

      expect(response).to be_successful
      expect(response).to have_http_status(:no_content)
    end

    it 'negative amount JSON' do
      post url, params: {
        contribution: { user_id: contributor.id, amount_cents: -10_000 }
      }

      expect(response).not_to be_successful
      expect(response).to have_http_status(:unprocessable_entity)

      expect(json.keys).to match_array(['errors'])
      expect(json['errors'].length).to be > 0
    end

    it 'error JSON' do
      post url, params: { contribution: { user_id: nil } }

      expect(response).not_to be_successful
      expect(response).to have_http_status(:unprocessable_entity)

      expect(json.keys).to match_array(['errors'])
      expect(json['errors'].length).to be > 0
    end
  end

  context 'destroy' do
    let(:url) { "/api/funds/#{fund.id}/contributions/#{fund.contributions.pluck(:id).sample}" }

    it 'json' do
      delete url

      expect(response).to be_successful
    end
  end
end
