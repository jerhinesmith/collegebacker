require 'rails_helper'

describe 'api/v1/funds', type: :request do
  let(:current_user) { FactoryBot.create(:user) }

  before(:each) do
    set_user current_user
    set_api_version 1

    @funds = FactoryBot.create_list(:fund, 3, user: current_user)
  end

  context 'index' do
    let(:url) { '/api/funds' }

    it 'json' do
      get url

      expect(response).to be_successful
      expect(json.length).to eq 3
    end
  end
end
