require 'rails_helper'

describe 'api/v1/sessions', type: :request do
  let(:current_user) { FactoryBot.create(:user, password: 'password', password_confirmation: 'password') }

  context 'create' do
    let(:url) { '/api/login' }

    it 'success' do
      post url, params: {
        session: {
          email: current_user.email,
          password: 'password'
        }
      }

      expect(response).to be_successful
    end

    it 'failure' do
      post url, params: {
        session: {
          email: current_user.email,
          password: 'wrong password'
        }
      }

      expect(response).to_not be_successful
      expect(response).to have_http_status(:unauthorized)
    end
  end
end
