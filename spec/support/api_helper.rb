module Requests
  module ApiHelpers
    def set_api_version(version)
      @version = version.to_s.match?(/^v/i) ? version : "v#{version}"
    end

    def set_user(user)
      @user = user
    end

    def get(uri, params = {}, session = {})
      super uri, format_params(params, session)
    end

    def post(uri, params = {}, session = {})
      super uri, format_params(params, session)
    end

    def put(uri, params = {}, session = {})
      super uri, format_params(params, session)
    end

    def patch(uri, params = {}, session = {})
      super uri, format_params(params, session)
    end

    def delete(uri, params = {}, session = {})
      super uri, format_params(params, session)
    end

    private

    def format_params(params, session)
      headers = default_header.merge(session)
      params.merge(headers)
    end

    def default_header
      accept = "application/vnd.#{ApiConstraints::VENDOR_PREFIX}.#{@version || 'v1'}"

      header = { 'Accept' => accept }

      header[:Authorization] = "Token token=\"#{@user.authentication_token}\"" if @user

      { headers: header }
    end
  end
end
